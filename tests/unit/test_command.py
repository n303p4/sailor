"""Test basic command functionality."""

# pylint: disable=missing-function-docstring

import string
from unittest import mock

import pytest

import sailor
from sailor import commands, exceptions


@sailor.command()
async def ping(event):
    await event.reply("Pong!")


@ping.command()
async def pong(event):
    await event.reply("Ping pong!")


@commands.command()
async def needint(event, arg: int):
    await event.reply(type(arg).__name__)
    await event.reply(arg)


@commands.command()
async def error(event):
    raise Exception()


@commands.command(owner_only=True)
async def owner(event):
    await event.reply("Is owner.")


@owner.command(owner_only=True)
async def ownerchild(event):
    await event.reply("Is owner's child.")


@commands.command()
async def multiarg(event, *args):
    for arg in args:
        await event.reply(arg)


TEST_DATA = [
    ("ping", ping, True, True, None, ["Pong!"]),  # Basic command
    ("ping pong", ping, True, True, None, ["Ping pong!"]),  # Subcommand
    ("pingpong", ping, False, True, exceptions.CommandNotFound, []),  # Subcommand does not get called with no space
    ("error", error, False, True, exceptions.CommandError, []),  # CommandError
    ("needint", needint, False, True, exceptions.CommandError, []),  # Missing required argument
    ("needint a", needint, False, True, exceptions.BadArgument, []),  # Bad argument
    ("needint 1", needint, True, True, None, ["int", "1"]),  # Good argument
    ("owner", owner, False, False, exceptions.NotBotOwner, []),  # Not bot owner
    ("owner", owner, True, True, None, ["Is owner."]),  # Bot owner
    ("owner ownerchild", owner, False, False, exceptions.NotBotOwner, []),  # Child commands of bot owner inherit
    ("owner ownerchild", owner, True, True, None, ["Is owner's child."]),  # Child commands of bot owner inherit

    # Multi-argument command tests
    ("multiarg a+1 b;2 c_3", multiarg, True, True, None, ["a+1", "b;2", "c_3"]),
    ("multiarg 'a 1' 'b 2' c3", multiarg, True, True, None, ["a 1", "b 2", "c3"])
]


@pytest.mark.parametrize(
    "text,command,send_called,is_owner,exception_type,expected_reply_stack",
    TEST_DATA
)
def test_command(text, command, send_called, is_owner, exception_type, expected_reply_stack):
    send = mock.MagicMock()
    reply_stack = []

    async def send_wrapper(reply):
        send(reply)
        reply_stack.append(reply)

    processor = commands.Processor()
    processor.add_command(command)

    if exception_type:
        with pytest.raises(exception_type):
            processor.loop.run_until_complete(
                processor.process(text, reply_with=send_wrapper, is_owner=is_owner)
            )
    else:
        processor.loop.run_until_complete(
            processor.process(text, reply_with=send_wrapper, is_owner=is_owner)
        )
    processor.loop.run_until_complete(processor.logout())
    assert send.called == send_called
    assert reply_stack == expected_reply_stack


@pytest.mark.parametrize("character", string.punctuation)
def test_punctuation(character):
    if character in "'\"":
        return

    send = mock.MagicMock()
    reply_stack = []

    async def send_wrapper(reply):
        send(reply)
        reply_stack.append(reply)

    processor = commands.Processor()
    processor.add_command(multiarg)

    processor.loop.run_until_complete(
        processor.process(
            f"multiarg a{character}b a{character}{character}b",
            reply_with=send_wrapper
        )
    )
    processor.loop.run_until_complete(processor.logout())
    assert reply_stack == [f"a{character}b", f"a{character}{character}b"]

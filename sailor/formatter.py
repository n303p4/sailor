"""Text formatting base class. In usage, you can call this via event.f"""


class BaseTextFormatter:
    """A base class for formatting text.

    You should subclass this to suit the behavior of whatever service you use.
    """

    def codeblock(self, text, syntax: str = ""):
        """Wrap some text in a codeblock."""
        return str(text)

    def bold(self, text):
        """Return bolded text."""
        return str(text)

    def italic(self, text):
        """Return italic text."""
        return str(text)

    def underline(self, text):
        """Return underlined text."""
        return str(text)

    def monospace(self, text):
        """Return monospace text."""
        return str(text)

    def no_embed_link(self, text):
        """Return a URL formatted so that it is not embedded."""
        return str(text)

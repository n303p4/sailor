"""Abstract channel dataclasses, to be subclassed by a frontend to a chat service."""

from abc import ABC, abstractmethod


class AbstractChannel(ABC):
    """An abstraction of a communications channel with a chat service (e.g. a Discord or IRC channel)."""

    def __init__(self, **kwargs):
        name = kwargs.get("name")
        self.__name = name if isinstance(name, str) else "untitled"

    @property
    def name(self):
        """The channel's name. Read-only."""
        return self.__name

    @abstractmethod
    async def edit(self, **kwargs):
        """Edit the channel's properties."""


class SimpleChannel(AbstractChannel):
    """An abstraction of a communications channel with a chat service (e.g. a Discord or IRC channel).

    This implementation doesn't do anything of particular use.
    """

    async def edit(self, **kwargs):
        """Edit the channel's properties."""

"""General exceptions for sailor."""


class SailorError(Exception):
    """Base class for all sailor-related errors."""
    def __init__(self, message: str = "sailor exception", error=None):
        super(SailorError, self).__init__()
        self.message = message
        self.error = error

    def __str__(self):
        return (f"{self.__class__.__name__}: {self.message}: "
                f"{self.error.__class__.__name__}: {self.error}")


class NotCoroutine(SailorError):
    """Raised if a command or channel instance is created with a function that is not a coroutine."""
    def __str__(self):
        return f"{self.__class__.__name__}: {self.message}"


class CommandError(SailorError):
    """Base class for errors that occur on command invocation, within a command."""
    def __init__(self, message: str = None, error=None):
        super(CommandError, self).__init__(message, error)


class UserInputError(CommandError):
    """Raise this if the user inputs something wrongly."""
    def __init__(self, message: str = "You entered something wrongly."):
        super(UserInputError, self).__init__(message)

    def __str__(self):
        return f"{self.__class__.__name__}: {self.message}"


class OnCooldown(CommandError):
    """Raised if a command is invoked while on cooldown."""
    def __init__(self, message: str = "Command is on cooldown."):
        super(OnCooldown, self).__init__(message)

    def __str__(self):
        return f"{self.__class__.__name__}: {self.message}"


class BadArgument(CommandError):
    """Raised if a bad argument is supplied to a command."""
    def __init__(self, message: str = "Invalid argument supplied."):
        super(BadArgument, self).__init__(message)

    def __str__(self):
        return f"{self.__class__.__name__}: {self.message}"


class NotBotOwner(CommandError):
    """Raised if an owner-only command is invoked by someone who isn't the bot owner."""
    def __str__(self):
        return f"{self.__class__.__name__}: {self.message}"


class FeatureNotImplemented(CommandError):
    """Raised if a feature is not implemented."""
    def __str__(self):
        return f"{self.__class__.__name__}: {self.message} is not implemented in the bot"


class CommandProcessorError(SailorError):
    """Base class for errors that occur outside the command level, but within a processor instance."""
    def __init__(self, message: str = "Command processor exception", error=None):
        super(CommandProcessorError, self).__init__(message, error)


class CommandNotFound(CommandProcessorError):
    """This exception should be raised if a web API cannot be reached."""
    def __init__(self, *, name: str = None):
        self.name = name
        super(CommandNotFound, self).__init__(f"Command {self.name} does not exist")

    def __str__(self):
        return f"{self.__class__.__name__}: {self.message}"


class InvalidMessage(CommandProcessorError):
    """Inbound message is not valid."""
    def __str__(self):
        return f"{self.__class__.__name__}: {self.message} is not a valid message"


class CommandExists(CommandProcessorError):
    """Raised when attempting to register a command with a name that already exists in the processor instance."""
    def __str__(self):
        return f"{self.__class__.__name__}: {self.message}"

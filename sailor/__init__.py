# pylint: disable=C0103
"""sailor is a Python library that provides command handling for use in bots."""

from sailor.commands import command, http_client
from sailor.formatter import BaseTextFormatter
import sailor.discord_helpers as discord_helpers

__version__ = (0, 0, 2, "a", "Mercury")
version = '{0}.{1}.{2}{3} "{4}"'.format(*__version__)

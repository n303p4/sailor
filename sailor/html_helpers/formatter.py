"""HTML text formatter for sailor."""

import sailor


class TextFormatter(sailor.BaseTextFormatter):
    """Discord text formatter for sailor."""

    def codeblock(self, text, _):
        stripped_text = str(text).strip("\n")
        return f"<pre><code>{stripped_text}</code></pre>"

    def bold(self, text):
        return f"<span style='font-weight: bold;'>{text}</span>"

    def italic(self, text):
        return f"<span style='font-style: italic;'>{text}</span>"

    def underline(self, text):
        return f"<span style='text-decoration: underline;'>{text}</span>"

    def no_embed_link(self, text):
        return f"<a href='{text}'>{text}</a>"

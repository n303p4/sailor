"""Discord text formatter for sailor."""

import sailor


class TextFormatter(sailor.BaseTextFormatter):
    """Discord text formatter for sailor."""

    def codeblock(self, text, syntax: str = ""):
        stripped_text = str(text).strip("\n")
        return f"```{syntax}\n{stripped_text}\n```"

    def bold(self, text):
        return f"**{text}**"

    def italic(self, text):
        return f"*{text}*"

    def underline(self, text):
        return f"__{text}__"

    def monospace(self, text):
        return f"`{text}`"

    def no_embed_link(self, text):
        return f"<{text}>"

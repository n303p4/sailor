"""sailor is a small and basic command handling framework for making Python bots."""

import asyncio
from datetime import timedelta
import importlib
import inspect
import os
import logging
import shlex
import string
import sys
import time
import traceback
from typing import List, Union

try:
    import httpx
except ImportError:
    pass

import sailor
from sailor.channel import AbstractChannel, SimpleChannel
import sailor.formatter
import sailor.exceptions

WORDCHARS = (string.digits + string.ascii_letters + string.punctuation).replace('"', "").replace("'", "")

logger = logging.getLogger(__name__)  # pylint: disable=C0103


def text_to_command_arguments(text: str, signature: inspect.Signature, *,
                              skip_event: bool = False, wordchars: str = WORDCHARS):
    """Parse a text string into arguments based on a function's signature. Return a tuple where
    the first member of the tuple is a `list` containing non-keyword arguments, and the second
    part is a `dict` containing a lone keyword argument, which can be used to "collect" leftover
    text into a single argument. Multiple keyword arguments are unsupported.

    * `text` - The text string to be processed.
    * `signature` - An `inspect.Signature` that is used as the basis for text parsing.
    * `skip_event` - If `True`, skip over the `event` argument that is required by sailor command
                     coroutines.
    """
    args = []
    kwargs = {}
    parser = shlex.shlex(text)
    parser.wordchars = wordchars
    parser.commenters = ""
    signature_parameters = list(signature.parameters.values())

    if skip_event:
        start = 1
    else:
        start = 0

    for parameter in signature_parameters[start:]:
        if parameter.kind == parameter.KEYWORD_ONLY:
            collector = parser.instream.read()
            if collector:
                kwargs[parameter.name] = collector
            break
        elif parameter.kind == parameter.VAR_POSITIONAL:
            while True:
                token = parser.get_token().strip(parser.quotes)
                if not token:
                    break
                args.append(token)
        else:
            token = parser.get_token().strip(parser.quotes)
            if token and parameter.annotation is not parameter.empty:
                try:
                    args.append(parameter.annotation(token))
                except ValueError as error:
                    raise sailor.exceptions.BadArgument((
                        f"Argument named \"{parameter.name}\" must "
                        f"be of type {parameter.annotation.__name__}."
                    )) from error
            elif token:
                args.append(token)

    return args, kwargs


class CommandGroupMixin:
    """This class contains partial command handling facilities and command grouping
    functionality. You should not use this by itself, though you may want to subclass it for
    your uses.

    Both `Processor` and `Command` inherit from this class.

    * `commands` - A `dict` containing all of the mixin's child commands.
    * `all_commands` - A `dict` containing all of the mixin's child commands, including aliases.
    * `name` - An `str` representing the name of the `CommandGroupMixin`. Children classes
               should reimplement `__init__` to set this as desired. `name` is used in `process`
               to detect command invocations; a matching `name` will trigger the corresponding
               command.
    * `aliases` - A `list` of `str` representing alternates to `name` for command invocations.
    * `parent` - The `Command` or `Processor` that the mixin instance has been added to. This will
                 be `None` if the instance is at the top level.
    """

    def __init__(self, *, name: str = None, aliases: List[str] = None):
        self.commands = {}
        self.all_commands = {}
        self.name = name or self.__name__
        self.aliases = aliases or []
        self.parent = None

    def add_command(self, command_, *, skip_duplicate=False):
        """Add a child `Command` instance to the mixin. This sets the child's parent to the mixin.

        Normally, you do not call this by itself; instead, you use the `CommandGroupMixin.command`
        decorator. Alternatively, you may call `Processor.add_module()` on a module that contains
        a collection of `Command` instances created using the `command()` decorator.

        * `command_` - A `Command` instance to be added.
        * `skip_duplicate` - A `bool`. If `True`, the function returns immediately if the mixin
                             already has a child with the same name as `command`. Otherwise, it
                             raises a `CommandExists` exception. Defaults to `False`.
        """
        is_registered = command_.is_registered_to(self)

        if is_registered and skip_duplicate:
            return
        elif is_registered:
            raise sailor.exceptions.CommandExists((f"{command_.name} is a command "
                                                   "that already exists."))

        command_.parent = self

        self.commands[command_.name] = command_
        self.all_commands[command_.name] = command_

        for alias in command_.aliases:
            self.all_commands[alias] = command_

    def remove_command(self, name):
        """Remove a `Command` object from the group, by name. This also sets the object's parent
        to `None`.

        * `name` - An `str` referring to the name of the command to remove.
        """
        aliases = self.commands[name].aliases
        self.commands[name].parent = None
        del self.commands[name]
        del self.all_commands[name]
        for alias in aliases:
            del self.all_commands[alias]

    def command(self, *, name: str = None, aliases: List[str] = None,
                owner_only: bool = False):
        """This is a shortcut decorator that directly adds a child command to the mixin.

        Refer to the `command()` decorator for more details, as it is functionally similar.
        """

        def decorator(coro):
            """The actual command object."""
            command_ = Command(coro, name=name, aliases=aliases, owner_only=owner_only)
            self.add_command(command_)
            return command_

        return decorator

    async def invoke(self, event, arguments):
        """Calling this will invoke the command, or one of its child commands, if applicable.

        * `event` - An `Event` representing the event that triggered the invocation.
        * `arguments` - A text string to be parsed into arguments.
        """
        if not self.all_commands:
            await self._invoke(event, arguments)
            return

        command_name, _, remaining_arguments = arguments.partition(" ")
        child_command = self.all_commands.get(command_name)
        if child_command:
            event.command = child_command
            event.invoked_with = command_name
            await child_command.invoke(event, remaining_arguments)
        else:
            await self._invoke(event, arguments)

    async def _invoke(self, event, arguments):
        """Perform actions if no child commands are found.

        Intended to be redefined in child classes.
        """

    @property
    def top_level(self):
        """Return the top-level instance for this object. Usually a `Processor`, unless you have
        defined a custom subclass.
        """
        current_level = self
        while current_level.parent:
            current_level = current_level.parent
        return current_level


class Processor(CommandGroupMixin):
    """A processor for Unix-like text-based commands. Individual commands and their child commands
    are represented by `Command` objects.

    To use this, instantiate it and call `Processor.process(text)` whenever a text message event
    occurs.

    Inherits `CommandGroupMixin`; refer to that class for further details.

    * `loop` - An `asyncio.AbstractEventLoop` to pass to the command processor. Defaults to the
               default `asyncio` loop if `None`.
    * `name` - An `str` representing the name of the processor.
    * `description` - An `str` representing the description of the processor.
    * `logout` - An optional callable parameter that allows for an abstracted bot logout. This
                 allows you to supply a method for cleanly exiting the bot. It can be as simple
                 as supplying `sys.exit`, though this will usually not be a clean exit. The
                 `logout` parameter may be a coroutine function.

    Instance variables not in the constructor:

    * `session` - An `httpx.AsyncClient` that the `Processor` and its commands can use to
                  make HTTP requests. This is useful for commands that make calls to web-based
                  APIs, such as to Wikipedia. If `aiohttp` is not available, this is just `None`.
    * `headers` - Default HTTP headers for the Processor, which are used in all HTTP requests.
                  Normally these contain a user agent.
    * `modules` - A `dict` of modules that the processor currently has loaded.

    Example usage:

        import sailor

        processor = sailor.commands.Processor(name="MyBot")
    """

    instance = None

    def __init__(self, *, loop: asyncio.AbstractEventLoop = None,
                 name: str = "sailor",
                 description: str = "A bot made using the sailor command handler.", logout=None):
        super(Processor, self).__init__(name=name)
        self.instance = self
        if not loop:
            loop = asyncio.get_event_loop()
        self.loop = loop
        version_string = f"{sailor.__version__[0]}.{sailor.__version__[1]}.{sailor.__version__[2]}"
        self.headers = {"User-Agent": f"sailor/{version_string}"}
        self.description = description
        self._logout = logout
        self.default_formatter = sailor.formatter.BaseTextFormatter()
        self.formatters = {}

        self.modules = {}

    @property
    def session(self):
        """Convenience method that creates a client object for HTTP persistent connections."""
        return httpx.AsyncClient(headers=self.headers)

    def register_formatter(self, formatter, format_name):
        """Add to the mapping of formatters in the handler."""
        self.formatters[format_name] = formatter

    async def logout(self):
        """An abstracted logout method. This is a coroutine.

        You must specify the logout function in the `Processor` constructor.
        """
        await self.session.aclose()
        if asyncio.iscoroutinefunction(self._logout):
            await self._logout()
        elif callable(self._logout):
            self._logout()

    async def process(self, message: str, *, reply_with, sleep_with = None,
                      channel: AbstractChannel = None, author_id: str = None, is_owner: bool = False,
                      character_limit: int = 2000, replace_newlines: bool = False,
                      format_name: str = None):
        """Execute the processor on an arbitrary string of text. This is a coroutine.

        Refer to `Event` for more details on the parameters.
        """
        if not message or not isinstance(message, str):
            raise sailor.exceptions.InvalidMessage(message)

        command_name, _, arguments = message.partition(" ")
        child_command = self.all_commands.get(command_name)

        if child_command:
            try:
                formatter = self.formatters.get(format_name)
                if not isinstance(channel, AbstractChannel):
                    channel = SimpleChannel()
                event = Event(callback_reply=reply_with, callback_sleep=sleep_with, processor=self,
                              channel=channel, command_=child_command, message=message,
                              character_limit=character_limit, invoked_with=command_name,
                              author_id=author_id, is_owner=is_owner, replace_newlines=replace_newlines,
                              formatter=formatter)
                await child_command.invoke(event, arguments)
            except sailor.exceptions.SailorError as error:
                logger.error(traceback.format_exc())
                raise error
            except Exception as error:
                command_processor_error = sailor.exceptions.CommandProcessorError(
                    "Command processor raised an error",
                    error
                )
                logger.error(traceback.format_exc())
                raise command_processor_error from error
        else:
            raise sailor.exceptions.CommandNotFound(name=command_name)

    def add_module(self, name: str, *, package: str = None, skip_duplicate_commands: bool = False):
        """Add a Python module to the command processor, by name.

        The processor will check said module for instances of `Command` and add all of them to
        itself.

        If the module has a function called `setup`, this function will call it automatically
        after the commands are added, with the processor instance as its argument. This allows
        additional setup steps to be performed.

        * `name` - An `str` representing the name of the module to import.
        * `skip_duplicate_commands` - A `bool`. If `True`, the function will skip command names
                                      that are already registered. Otherwise, it raises a
                                      `CommandExists` exception. Defaults to `False`.
        """

        # If the module is already in memory, reload it instead.
        if name in sys.modules:
            importlib.reload(sys.modules[name])
            module = sys.modules[name]
        else:
            module = importlib.import_module(name, package=package)

        self.modules[name] = module

        module_attributes = dir(module)

        for attribute in module_attributes:

            item = getattr(module, attribute)

            if isinstance(item, Command) and not item.parent:
                self.add_command(item, skip_duplicate=skip_duplicate_commands)

        if "setup" in module_attributes and callable(module.setup):
            try:
                module.setup(self)
            except Exception as error:
                logger.error(traceback.format_exc())
                raise error

    def add_modules_from_dir(self, directory: str, *, blocklist: List[str] = None,
                             package: str = None, skip_duplicate_commands: bool = False):
        """Automatically load all modules from a directory and its subdirectories.

        See `Processor.add_module`.
        """
        if not blocklist:
            blocklist = []

        for dirpath, _, filenames in os.walk(directory):
            for filename in filenames:
                if filename.endswith(".py"):
                    fullpath = os.path.join(dirpath, filename).split(os.sep)
                    module = ".".join(fullpath)[:-3]  # Eliminate the .py
                    if module in blocklist:  # Skip blocklisted modules.
                        continue
                    try:
                        self.add_module(module, package=package, skip_duplicate_commands=skip_duplicate_commands)
                    except Exception:  # pylint: disable=broad-except
                        logger.error(traceback.format_exc())

    def remove_module(self, name):
        """Remove a Python module from the command processor, by name.

        When this is called, the processor will remove all `Command` objects that belong to the
        module in question. It does not unload the module from memory, as Python doesn't support
        this.

        If the module has a function called `unload`, this function will call it automatically
        after the commands are unloaded, with the processor instance as its argument. This allows
        us to do manual cleanup tasks.

        * `name` - An `str` representing the name of the module to remove.
        """
        module = self.modules[name]
        del self.modules[name]

        for command_name, command_ in tuple(self.commands.items()):
            if command_.coro.__module__ == name:
                self.remove_command(command_name)

        module_attributes = dir(module)
        if "unload" in module_attributes and callable(module.unload):
            try:
                module.unload(self)
            except Exception as error:
                logger.error(traceback.format_exc())
                raise error


class Command(CommandGroupMixin):
    """This object represents a command that a command processor can use. It can also have child
    commands.

    Normally, you do not construct this directly. Use the decorator syntax instead.

    * `coro` - A coroutine function that the command uses upon calling `invoke()`.
    * `name` - An `str` representing the name for the command. If unspecified, it is set to
               `coro.__name__`.
    * `aliases` - A `list` of `str` representing aliases for the command; that is, alternative
                  names you can invoke the command under.
    * `owner_only` - A `bool`. Determines whether the command is meant for the bot owner only.
                     If `True`, the command will require some sort of override in order to run,
                     to be provided by implementation.

    Instance variables not in the constructor:

    * `help` - An `str` representing the command's help text. It is read automatically from
               the coroutine's docstring, so you do not need to specify it manually.
    * `top_level` - Usually the `Processor` instance that the command is assigned to.
    * `signature` - An `inspect.Signature` reflecting the command signature.
    """

    def __init__(self, coro, *, name: str = None, aliases: List[str] = None,
                 owner_only: bool = False):
        name = name or coro.__name__
        super(Command, self).__init__(name=name, aliases=aliases)

        self.help = inspect.getdoc(coro)

        if not asyncio.iscoroutinefunction(coro):
            raise sailor.exceptions.NotCoroutine(f"{coro.__name__} is not a coroutine function.")
        self.coro = coro
        self.signature = inspect.signature(self.coro)

        self.owner_only = owner_only

        # These are used internally for cooldowns.
        self._interval_start = 0  # This tracks the start of an interval.
        self._times_invoked = 0  # This tracks the number of times the command is used.
        self._limit = 0  # Limit of uses per interval
        self._interval = 0  # Interval in seconds

    def set_cooldown(self, limit: int, interval: Union[int, float, timedelta] = 1):
        """This sets the cooldown for the command.

        Normally, you do not call this directly; use the decorator syntax instead.

        * `limit` - An `int` representing the limit of uses per interval before the cooldown kicks.
        * `interval` - An `int`, `float`, or `timedelta` representing, in seconds, the interval before the
                       cooldown resets. Defaults to `1`.
        """
        self._limit = limit
        if isinstance(interval, timedelta):
            interval = interval.total_seconds()
        self._interval = interval

    def _update_cooldown(self):
        if self._limit <= 1:
            return
        invoke_time = time.time()
        if invoke_time - self._interval_start >= self._interval:
            self._times_invoked = 1
            self._interval_start = invoke_time
        elif self._times_invoked >= self._limit:
            raise sailor.exceptions.OnCooldown("Command on cooldown.")
        else:
            self._times_invoked += 1

    async def invoke(self, event, arguments):
        """Overridden to intercept child commands if owner_only is set."""
        if self.owner_only and not event.is_owner:
            raise sailor.exceptions.NotBotOwner("You don't own this bot.")
        await super().invoke(event, arguments)

    async def _invoke(self, event, arguments):
        """Perform actions if no child commands are found.

        Overridden to handle argument parsing and perform standalone actions.
        """
        self._update_cooldown()

        if self.owner_only and not event.is_owner:
            raise sailor.exceptions.NotBotOwner("You don't own this bot.")

        try:
            args, kwargs = text_to_command_arguments(arguments,
                                                     self.signature,
                                                     skip_event=True)
            await self.coro(event, *args, **kwargs)
        except sailor.exceptions.SailorError as error:
            raise error
        except TypeError as error:
            required_arguments = []
            for parameter in list(self.signature.parameters.values())[1:]:
                if parameter.default == inspect.Parameter.empty:
                    required_arguments.append(parameter.name)
            raise sailor.exceptions.UserInputError(
                f"The following arguments are required for this command: {', '.join(required_arguments)}"
            ) from error
        except Exception as error:
            command_error = sailor.exceptions.CommandError(
                f"Command {self.name} raised an error", error
            )
            raise command_error from error

    def is_registered_to(self, parent):
        """Check if this command is registered to a parent."""
        return self in parent.commands.values()


class Event:
    """Representation of an event when a `Command` is invoked. You should not have to create these
    manually.

    * `callback_reply` - A coroutine for replying to an event with a message. Call it using
                         `event.reply()`.
    * `command_` - The `Command` instance associated with the event.
    * `processor` - The `Processor` instance associated with the event.
    * `channel` - A `Channel` instance for interacting with the chat service.
    * `invoked_with` - `str` representing the name that invoked the command (and event).
    * `character_limit` - An `int` representing the maximum allowable characters per message.
                          The `Event.reply` method will automatically split messages that
                          are too long; if this behavior is undesirable, you should do a manual
                          truncation. If this is set to 0 or a negative number, then no message
                          splitting is performed.
    * `author_id` - An arbitrary `str` value that represents the ID of the user who started the event.
    * `is_owner` - A `bool` indicating whether the person who invoked the command is the bot's
                   owner.
    * `replace_newlines` - A `bool` indicating whether to replace newlines, for services that don't
                           support newlines (e.g. Twitch). If true, all newlines are replaced with
                           " | " and repeating newlines are stripped.
    * `formatter` - A `BaseTextFormatter` instance, used to format text for given chat services.
    """

    def __init__(self, *, callback_reply, message: str, command_: Command,
                 processor: Processor, invoked_with: str, callback_sleep = None,
                 channel: AbstractChannel = None, character_limit: int = 2000,
                 author_id: str = None, is_owner: bool = False, replace_newlines: bool = False,
                 formatter: sailor.formatter.BaseTextFormatter = None):
        if not asyncio.iscoroutinefunction(callback_reply):
            raise sailor.exceptions.NotCoroutine((f"{callback_reply.__name__} is not a "
                                                  "coroutine function."))
        self.__callback_reply = callback_reply
        self.__callback_sleep = callback_sleep or asyncio.sleep
        self.processor = processor
        self.author_id = str(author_id) if author_id else None
        self.channel = channel if isinstance(channel, AbstractChannel) else SimpleChannel()
        self.formatter = formatter or processor.default_formatter
        self.message = message
        self.command = command_
        self.character_limit = character_limit
        self.invoked_with = invoked_with
        self.is_owner = is_owner
        self.replace_newlines = replace_newlines

    @property
    def text_formatter(self):
        """Alias for event.formatter."""
        return self.formatter

    @property
    def f(self):  # pylint: disable=C0103
        """Alias for event.formatter."""
        return self.formatter

    async def reply(self, message):
        """An abstracted method that replies to the event with a message. This is a coroutine.

        The actual reply method is passed in through the constructor.

        Supports auto-pagination using the specified character limit.
        """
        message = str(message)
        if self.replace_newlines:
            message = " | ".join(list(filter(None, message.split("\n"))))
        character_limit = self.character_limit
        if character_limit > 0:
            pages = [
                message[i:i+character_limit] for i in range(0, len(message), character_limit)
            ]
            for page in pages:
                await self.__callback_reply(page)
        else:
            await self.__callback_reply(message)

    async def sleep(self, seconds: float):
        """An abstracted method that tells the command to sleep for some number of seconds.

        Defaults to using `asyncio.sleep`.        
        """
        if not isinstance(seconds, (float, int)):
            raise TypeError(f"{seconds} is not a valid number of seconds!")
        if asyncio.iscoroutinefunction(self.__callback_sleep):
            await self.__callback_sleep(seconds)
        elif callable(self._logout):
            self.__callback_sleep(seconds)


def http_client():
    """A convenience method that returns an `httpx.AsyncClient` based on the last created `Processor` instance.

    If you only ever use one `Processor` instance, then this may simplify your code.
    """
    return Processor.instance.session


def command(*, name: str = None, aliases: List[str] = None, owner_only: bool = False):
    """This is a decorator, which you call on a coroutine to make it into a `Command` object.

    Normally, you should use this instead of constructing `Command` objects directly.

    * `coro` - A coroutine function that the command uses upon calling `invoke()`. When a command
               is invoked, argument parsing is performed on the message string that invoked it,
               based on the `inspect.Signature` of this coroutine. The first argument to the
               coroutine should always be a `Event` object.
    * `name` - An `str` representing the name for the command. If unspecified, it is set to
               `coro.__name__`.
    * `aliases` - A `list` of `str` representing aliases for the command; that is, alternative
                  names you can invoke the command under.
    * `owner_only` - A `bool`. Determines whether the command is meant for the bot owner only.
                     If `True`, the command will require `is_owner` to be overridden in the call
                     to the `invoke()` method. Defaults to `False`.

    Simple example:

        @sailor.command()
        async def ping(event):
            await event.reply("Pong!")

    More sophisticated example:

        @sailor.command()
        async def echo(event, *, collector):
            '''Help text is automatically read from docstrings.'''
            await event.reply(collector)
    """

    def decorator(coro):
        """The actual command object."""
        command_ = Command(coro, name=name, aliases=aliases, owner_only=owner_only)
        return command_

    return decorator


def cooldown(limit: int, interval: Union[int, float, timedelta] = 1):
    """This is a decorator that sets the cooldown for a command.

    Normally, you should use this instead of calling `Command.set_cooldown()` directly.

    * `limit` - An `int` representing the limit of uses per interval before the cooldown kicks.
    * `interval` - An `int`, `float`, or `timedelta` representing, in seconds, the interval before
                   the cooldown resets. Defaults to `1`.

    Example:

        @sailor.commands.cooldown(6, 12)  # Allow 6 uses per 12 seconds.
        @sailor.command()
        async def ping(event):
            await event.reply("Pong!")
    """

    def decorator(cmd):
        """The command object, with the cooldown applied to it."""
        cmd.set_cooldown(limit, interval)
        return cmd

    return decorator

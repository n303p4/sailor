#!/usr/bin/env python3

"""Setuptools script."""

from setuptools import setup

setup(name="sailor",
      version="0.0.2a",
      description="A bot command handling framework in Python",
      license="MIT",
      packages=["sailor", "sailor.discord_helpers"],
      zip_safe=False,
      extras_require={"http": ["aiohttp"]})

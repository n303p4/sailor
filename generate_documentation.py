#!/usr/bin/env python3
# pylint: disable=C0103

"""
Generate documentation for sailor.commands.

Depends on pdoc3, not upstream pdoc.
"""

# https://stackoverflow.com/questions/33517072/

import pdoc

module = pdoc.import_module("sailor.commands")
documentation = pdoc.Module(module)
print(documentation.html())

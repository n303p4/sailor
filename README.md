# sailor

[![MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://github.com/n303p4/Kitsuchan-NG/blob/master/LICENSE.txt)
[![Python](https://img.shields.io/badge/Python-3.6-brightgreen.svg)](https://python.org/)

```py
import sailor

@sailor.command()
async def hello(event):
    await event.reply("Hello sailor!")
```

**sailor** (formerly known as **k3**) is a command handler and text bot framework written in
Python. It is not a full functioning bot by itself - instead, it is designed to be used in
conjunction with existing Python text chat wrappers, such as `irc` or `discord.py`. The idea is to
write a "common denominator" library that can be used to create simple command-oriented bots for
any chat service of your choosing.

sailor was mainly influenced by `cmd` and `discord.py`'s command handling extensions.

## Disclaimer

sailor is far from finished! Please be mindful of this before deciding to use it for one of your
projects. It's a moving target, and there will most likely be API-breaking changes in the future.

## Advantages

* Write commands once and use them with any chat service of your choosing.
* `asyncio`-based, enabling significant concurrency without the need for Python threads.
* Easy to use with other Python libraries.

## Disadvantages

* Harder to use with synchronous or `threading`-based wrappers.
* Lacks depth; highly-specialized tasks are impossible to handle with sailor alone.
* Requires abstractions to do even basic things such as sending messages and identifying users.

## FAQs

### How do I run sailor?

You don't, since sailor is a library for bots, not a standalone bot. Refer to the example bot
repository for a working Discord bot that uses sailor.

### Why do I get a syntax error?

Make sure your Python version is 3.6 or higher.

## License

Refer to `LICENSE.txt`.
